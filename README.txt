# hdr viewer
An HTML+JavaScript HDR image viewer, based on the output of pfstouthdrhtml tool from pfstools, allowing switching between multiple images.